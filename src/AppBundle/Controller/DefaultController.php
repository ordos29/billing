<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Expenses;
use AppBundle\Entity\ExpensesCategory;
use AppBundle\Entity\MonthLimit;
use AppBundle\Repository\MonthLimitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, EntityManagerInterface $em)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/expense", name="expense")
     */
    public function expenseAction(Request $request, EntityManagerInterface $em)
    {
        $expense = new Expenses();
        $expense->setDate(new \DateTime('now'));
        $categories = $this->getDoctrine()
            ->getRepository(ExpensesCategory::class)
            ->findAll();

        $form = $this->createFormBuilder($expense)
            ->add(
                'expenseCategory',
                ChoiceType::class,
                [
                    'label' => 'Категория расхода',
                    'choices' => $categories,
                    'choice_label' => function ($category, $key, $index) {
                        /** @var ExpensesCategory $category */
                        return $category->getName();
                    },
                    'choice_value' => function ($category) {
                        /** @var ExpensesCategory $category */
                        return $category ? $category->getId() : null;
                    },
                ])
            ->add('date', DateType::class, ['label' => 'Дата расхода', 'required' => true])
            ->add(
                'amount',
                MoneyType::class,
                [
                    'label' => 'Сумма расхода',
                    'divisor' => 100,
                    'currency' => false,
                    'required' => true
                ])
            ->add('save', SubmitType::class, ['label' => 'Добавить расход'])
            ->getForm();

        $form->handleRequest($request);

        $sumInMonth = $this->getDoctrine()
            ->getRepository(Expenses::class)
            ->findSumInMonth($expense->getDate()->format('Y-m') . '-01');

        $limitsRepository = new MonthLimitRepository();
        $limits = $limitsRepository->getMonthLimits();

        $message = '';
        if ($form->isSubmitted() && $form->isValid()) {
            $limit = $limitsRepository->getMonthLimitForMonth($expense->getMonth());
            if ($limit !== null && $sumInMonth + $expense->getAmount() > $limit->getSum()) {
                $nextMonth = clone $expense->getDate();
                $nextMonth->modify('+ 1 month');
                $nextMonthLimit = $limitsRepository->getMonthLimitForMonth($nextMonth->format('Y-m'));
                if ($limit->getLimitType() == MonthLimit::ADAPTIVE_LIMIT
                    && $nextMonthLimit !== null) {
                    $oldLimit = $nextMonthLimit->getSum() / 100;
                    $nextMonthLimit->decreaseLimit(($sumInMonth + $expense->getAmount()) - $limit->getSum());
                    $limits[$nextMonth->format('Y-m')];
                    $message = "При добавлении расхода был изменён лимит за месяц: {$nextMonth->format('Y-m')}, был {$oldLimit}, стал " . $nextMonthLimit->getSum() / 100;
                } elseif ($limit->getLimitType() == MonthLimit::INCREASE_LIMIT) {
                    $oldLimit = $limit->getSum() / 100;
                    $limit->setSum($sumInMonth + $expense->getAmount());
                    $limits[$expense->getMonth()] = $limit;
                    $message = "При добавлении расхода был изменён лимит за месяц: {$expense->getMonth()}, был {$oldLimit}, стал " . $limit->getSum() / 100;
                }

                $limitsRepository->saveMonthLimits($limits);
            }
            $em->persist($expense);
            $em->flush();
        }

        return $this->render('default/expense.html.twig', [
            'form' => $form->createView(),
            'message' => $message,
        ]);
    }

    /**
     * @Route("/summaryReport", name="summaryReport")
     */
    public function summaryReportAction(Request $request, EntityManagerInterface $em)
    {
        $expenses = $this->getDoctrine()
            ->getRepository(Expenses::class)
            ->findAll();

        $expensesMonth = [];
        foreach ($expenses as $expense) {
            if (isset($expensesMonth[$expense->getDate()->format('Y-m')])) {
                $expensesMonth[$expense->getDate()->format('Y-m')] += $expense->getAmount();
            } else {
                $expensesMonth[$expense->getDate()->format('Y-m')] = $expense->getAmount();
            }
        }
        ksort($expensesMonth);

        return $this->render('default/summaryreport.html.twig', [
            'expensesMonth' => $expensesMonth,
        ]);
    }

    /**
     * @Route("/detailReport", name="detailReport")
     */
    public function detailReportAction(Request $request, EntityManagerInterface $em)
    {
        $month = $request->get('month');

        if ($month) {
            $expenses = $this->getDoctrine()
                ->getRepository(Expenses::class)
                ->findAllInMonth($month . '-01');

            /** @var ExpensesCategory[] $categories */
            $categories = $this->getDoctrine()
                ->getRepository(ExpensesCategory::class)
                ->findAll();

            $expensesDays = [];
            $categoriesSums = [];
            foreach ($categories as $category) {
                $categoriesSums[$category->getName()] = 0;
            }
            foreach ($expenses as $expense) {
                $day = $expense->getDate()->format('d');
                $categoryName = $expense->getExpenseCategory()->getName();
                $categoriesSums[$categoryName] += $expense->getAmount();
                if (isset($expensesDays[$day])) {
                    $expensesDays[$day][$categoryName] += $expense->getAmount();
                    $expensesDays[$day]['Итого'] += $expense->getAmount();
                } else {
                    foreach ($categories as $category) {
                        $expensesDays[$day][$category->getName()] = 0;
                    }
                    $expensesDays[$day]['Итого'] = $expense->getAmount();
                    $expensesDays[$day][$categoryName] = $expense->getAmount();

                }
            }

            return $this->render('default/detailreport.html.twig', [
                'expensesDays' => $expensesDays,
                'categories' => $categories,
                'categoriesSums' => $categoriesSums,
            ]);
        }

        return $this->render('default/detailreportselect.html.twig');
    }
}
