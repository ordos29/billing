<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="expenses")
 */
class Expenses
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ExpensesCategory", inversedBy="expenses")
     * @ORM\JoinColumn(nullable=true, name="expense_category")
     */
    private $expenseCategory;

    /**
     * @ORM\Column(type="integer")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expenseCategory
     *
     * @param ExpensesCategory $expenseCategory
     *
     * @return Expenses
     */
    public function setExpenseCategory(ExpensesCategory $expenseCategory)
    {
        $this->expenseCategory = $expenseCategory;

        return $this;
    }

    /**
     * Get expenseCategory
     *
     * @return ExpensesCategory
     */
    public function getExpenseCategory()
    {
        return $this->expenseCategory;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Expenses
     */
    public function setDate($date)
    {
        $this->date = $date->format('U');

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        $date = new \DateTime();
        $date->setTimestamp($this->date);

        return $date;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Expenses
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getMonth()
    {
        return $this->getDate()->format("Y-m");
    }

}

