<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ExpensesCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Expensescategory controller.
 *
 * @Route("expensescategory")
 */
class ExpensesCategoryController extends Controller
{
    /**
     * Lists all expensesCategory entities.
     *
     * @Route("/", name="expensescategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $expensesCategories = $em->getRepository('AppBundle:ExpensesCategory')->findAll();

        return $this->render('expensescategory/index.html.twig', array(
            'expensesCategories' => $expensesCategories,
        ));
    }

    /**
     * Creates a new expensesCategory entity.
     *
     * @Route("/new", name="expensescategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $expensesCategory = new Expensescategory();
        $form = $this->createForm('AppBundle\Form\ExpensesCategoryType', $expensesCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($expensesCategory);
            $em->flush();

            return $this->redirectToRoute('expensescategory_show', array('id' => $expensesCategory->getId()));
        }

        return $this->render('expensescategory/new.html.twig', array(
            'expensesCategory' => $expensesCategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a expensesCategory entity.
     *
     * @Route("/{id}", name="expensescategory_show")
     * @Method("GET")
     */
    public function showAction(ExpensesCategory $expensesCategory)
    {
        $deleteForm = $this->createDeleteForm($expensesCategory);

        return $this->render('expensescategory/show.html.twig', array(
            'expensesCategory' => $expensesCategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing expensesCategory entity.
     *
     * @Route("/{id}/edit", name="expensescategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ExpensesCategory $expensesCategory)
    {
        $deleteForm = $this->createDeleteForm($expensesCategory);
        $editForm = $this->createForm('AppBundle\Form\ExpensesCategoryType', $expensesCategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('expensescategory_edit', array('id' => $expensesCategory->getId()));
        }

        return $this->render('expensescategory/edit.html.twig', array(
            'expensesCategory' => $expensesCategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a expensesCategory entity.
     *
     * @Route("/{id}", name="expensescategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ExpensesCategory $expensesCategory)
    {
        $form = $this->createDeleteForm($expensesCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($expensesCategory);
            $em->flush();
        }

        return $this->redirectToRoute('expensescategory_index');
    }

    /**
     * Creates a form to delete a expensesCategory entity.
     *
     * @param ExpensesCategory $expensesCategory The expensesCategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ExpensesCategory $expensesCategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expensescategory_delete', array('id' => $expensesCategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
