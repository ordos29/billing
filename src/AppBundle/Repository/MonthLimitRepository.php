<?php

namespace AppBundle\Repository;


use AppBundle\Entity\MonthLimit;

class MonthLimitRepository
{
    protected $monthLimitsPath;
    protected $monthLimits;

    public function __construct()
    {
        $this->monthLimitsPath =
            __DIR__
            . DIRECTORY_SEPARATOR
            . ".."
            . DIRECTORY_SEPARATOR
            . "Resources"
            . DIRECTORY_SEPARATOR
            . "config"
            . DIRECTORY_SEPARATOR
            . "monthLimits";
    }

    /**
     * @return MonthLimit[]
     */
    public function getMonthLimits()
    {
        if (!empty($this->monthLimits)) {
            return $this->monthLimits;
        }

        try {
            $limitsFile = file_get_contents($this->monthLimitsPath);
        } catch (\Exception $e) {
            file_put_contents($this->monthLimitsPath, null);
            return [];
        }

        $limitsArray = explode("\n", $limitsFile);
        $limits = [];
        if (!empty($limitsArray)) {
            foreach ($limitsArray as $monthLimitString) {
                $monthLimit = explode(" ", $monthLimitString);
                $month = new \DateTime(array_shift($monthLimit));
                $limits[$month->format("Y-m")] = new MonthLimit($month, array_shift($monthLimit), array_shift($monthLimit));
            }
        }
        $this->monthLimits = $limits;

        return $this->monthLimits;
    }

    /**
     * @param MonthLimit[] $limitsArray
     * @return bool|int
     */
    public function saveMonthLimits($limitsArray)
    {
        $limitsToSave = [];
        foreach ($limitsArray as $limit) {
            $limitsToSave[] = $limit->getMonth()->format("Y-m") . " " . $limit->getLimitType() . " " . $limit->getSum();
        }

        return file_put_contents($this->monthLimitsPath, implode("\n", $limitsToSave));
    }

    /**
     * @param string $month
     * @return MonthLimit
     */
    public function getMonthLimitForMonth($month)
    {
        $limits = $this->getMonthLimits();
        if (key_exists($month, $limits)) {
            $limit = $limits[$month];
        } else {
            $limit = null;
        }

        return $limit;
    }
}