<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 09.05.18
 * Time: 14:42
 */

namespace AppBundle\Entity;


use DateTime;

class MonthLimit
{
    const ADAPTIVE_LIMIT = 1;
    const INCREASE_LIMIT = 2;

    /**
     * @var DateTime
     */
    protected $month;
    /**
     * @var string
     */
    protected $limitType;
    /**
     * @var int
     */
    protected $sum;

    /**
     * MonthLimit constructor.
     * @param DateTime $month
     * @param string $limitType
     * @param int $sum
     */
    public function __construct(DateTime $month, $limitType, $sum)
    {
        $this->month = $month;
        $this->limitType = $limitType;
        $this->sum = $sum;
    }

    /**
     * @return DateTime
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param DateTime $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return string
     */
    public function getLimitType()
    {
        return $this->limitType;
    }

    /**
     * @param string $limitType
     */
    public function setLimitType($limitType)
    {
        $this->limitType = $limitType;
    }

    /**
     * @return int
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param int $sum
     */
    public function setSum($sum)
    {
        $this->sum = $sum;
    }

    /**
     * @param int $decreaseSum
     */
    public function decreaseLimit($decreaseSum)
    {
        if ($this->sum - $decreaseSum < 0) {
            $this->sum = 0;
        } else {
            $this->sum -= $decreaseSum;
        }
    }
}